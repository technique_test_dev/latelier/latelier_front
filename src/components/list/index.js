export default{
  name: "PlayerList",
  props:{
    players:{
      type: Array,
      default: []
    },
    playerDetail:{
      type: Function
    }
  },
  data(){
    return {
      items: [],
      search: ''
    }
  },
  mounted(){
    this.items = this.players
  },
  watch:{
    players(){
      this.items = this.players
    },
    search(){
      let array_filter = this.search.split(' ')
      let output = this.players

      array_filter.forEach(r=>{
        output = output.filter(rs=> rs.firstname && rs.firstname.toLowerCase().includes(r.toLowerCase()) || rs.lastname && rs.lastname.toLowerCase().includes(r.toLowerCase()))
      })

      this.items = output

    }
  }
}