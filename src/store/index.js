import { createStore } from 'vuex'
import api from './api'

export default createStore({
  state: {
    players:[],
    player: {
      country:{},
      data:{},
    },
    stat: {},
  },
  mutations: {
    setPlayers:(state, players)=>state.players = players,
    setPlayer:(state, player)=>state.player = player,
    setStat:(state, stat)=>state.stat = stat,
  },
  actions: {

    players: async({commit})=>{
      api.get('player')
      .then(rs=>{
        commit('setPlayers', rs.data)
      })
      .catch(()=>{
        commit('setPlayers', [])
      })
    },
    player: async({commit},player)=>{
      api.get('player/'+player)
      .then(rs=>{
        commit('setPlayer', rs.data)
      })
      .catch(()=>{
        commit('setPlayer', {
          country:{},
          data:{},
        })
      })
    },
    stat: async({commit})=>{
      api.get('player/stats')
      .then(rs=> commit('setStat', rs.data))
      .catch(()=>{
        commit('setStat', {})
      })
    },
  },
  getters:{
    players:(state)=> state.players,
    player:(state)=> state.player,
    stat:(state)=> state.stat,
  },
  modules: {
  }
})
