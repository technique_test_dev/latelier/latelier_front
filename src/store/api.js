import axios from 'axios'

axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json; charset=utf-8',
  'Access-Control-Allow-Origin': true,
}

export default axios