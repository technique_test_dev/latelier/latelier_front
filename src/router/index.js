import { createRouter, createWebHashHistory, RouterView } from 'vue-router'

import Home from '../views/home.vue'

const routes = [
  {
    path: '/',
    component: RouterView,
    children:[
      {
        path: '',
        name: "Home",
        component: Home
      },
    ],
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
