import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import PlayerDetail from '@/components/detail/index.vue'

describe('PlayerDetail.vue', () => {
  it('renders props.player when passed', () => {
    const player = {
      "id": 52,
      "firstname": "Novak",
      "lastname": "Djokovic",
      "shortname": "N.DJO",
      "sex": "M",
      "country": {
        "picture": "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
        "code": "SRB"
      },
      "picture": "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
      "data": {
        "rank": 2,
        "points": 2542,
        "weight": 80000,
        "height": 188,
        "age": 31,
        "last": [1, 1, 1, 1, 1]
      }
    }
    const wrapper = shallowMount(PlayerDetail, {
      props: { player }
    })
    
    expect(wrapper.find("#first").text()).to.include(player.firstname)
    expect(wrapper.find("#last").text()).to.include(player.lastname)
  })
})
