import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import PlayerList from '@/components/list/index.vue'

describe('PlayerList.vue', () => {
  it('renders props.players when passed', () => {
    const players = [{
      "id": 52,
      "firstname": "Novak",
      "lastname": "Djokovic",
      "shortname": "N.DJO",
      "sex": "M",
      "country": {
        "picture": "https://data.latelier.co/training/tennis_stats/resources/Serbie.png",
        "code": "SRB"
      },
      "picture": "https://data.latelier.co/training/tennis_stats/resources/Djokovic.png",
      "data": {
        "rank": 2,
        "points": 2542,
        "weight": 80000,
        "height": 188,
        "age": 31,
        "last": [1, 1, 1, 1, 1]
      }
    }]
    const wrapper = shallowMount(PlayerList, {
      props: { players }
    })
    const fullname = players[0].firstname+" "+players[0].lastname
    expect(wrapper.find(".card-title").text()).to.include(fullname)
  })
})
